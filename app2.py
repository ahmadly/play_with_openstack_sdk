import configparser
import datetime
import ceilometerclient.client
from novaclient import client

config = configparser.ConfigParser()
config.read('config.ini')

RESOURCE_USAGE_METERS = ['memory.usage', 'cpu', 'disk.usage', 'network.incoming.bytes', 'network.outgoing.bytes']


def get_nova_client(version=2, username=None, password=None, project_id=None, auth_url=None, **kwargs):
    return client.Client(
        version=version,
        username=username,
        password=password,
        project_id=project_id,
        auth_url=auth_url,
        **kwargs
    )


def get_ceilometer_client(version=2, os_username=None, os_password=None, os_tenant_name=None, os_auth_url=None):
    return ceilometerclient.client.get_client(
        version=version,
        os_username=os_username,
        os_password=os_password,
        os_tenant_name=os_tenant_name,
        os_auth_url=os_auth_url,
    )


def get_instance_usage(ceilometer_client, _meter, query):
    return ceilometer_client.statistics.list(meter_name=_meter,
                                             groupby="resource_id",
                                             q=query,
                                             )


def get_servers_list(nova_client):
    """
    get all servers in openstack
    :param nova_client:
    :return:
    """
    return nova_client.servers.list(search_opts={'all_tenants': 1})


def ceilometer_query_generator(project_id: str,
                               start_datetime: datetime.datetime,
                               end_datetime: datetime.datetime) -> list:
    return [
        dict(field='project_id', op='eq', value=project_id),
        dict(field='timestamp', op='ge', value=start_datetime.strftime("%Y-%m-%dT%H:%M")),
        dict(field='timestamp', op='lt', value=end_datetime.strftime("%Y-%m-%dT%H:%M")),
    ]


_ceilometer_client = get_ceilometer_client(
    os_username=config.get(config.default_section, 'username'),
    os_password=config.get(config.default_section, 'password'),
    os_tenant_name='admin',
    os_auth_url=config.get(config.default_section, 'auth_url'),
)

_nova_client = get_nova_client(
    username=config.get(config.default_section, 'username'),
    password=config.get(config.default_section, 'password'),
    project_id=config.get(config.default_section, 'project_id'),
    auth_url=config.get(config.default_section, 'auth_url')
)

for server in get_servers_list(_nova_client):
    print('Name: {name}, ID: {id}'.format(name=server.name, id=server.tenant_id))
    _query = ceilometer_query_generator(project_id=server.tenant_id,
                                        start_datetime=datetime.datetime.today() - datetime.timedelta(days=1),
                                        end_datetime=datetime.datetime.today(),
                                        )
    _data = {}
    for meter in RESOURCE_USAGE_METERS:
        _usages = get_instance_usage(_ceilometer_client, _meter=meter, query=_query)
        for usage in _usages:
            print('resource_id:', server.id, 'data:', _data)
    print('Data: ', _data)
