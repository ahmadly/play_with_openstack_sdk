# Unlike most variables, the variable SHELL is never set from the environment.
# This is because the SHELL environment variable is used to specify your personal
# choice of shell program for interactive use. It would be very bad for personal
# choices like this to affect the functioning of makefiles. See Variables from the Environment.
SHELL=/bin/bash

# python config
PYTHONCOERCECLOCALE:=UTF-8
PYTHONUTF8:=1
PYTHONDEBUG:=1
PYTHONVERBOSE:=
PYTHONWARNINGS=always
PROJECT_VENV_DIRECTORY=venv
VIRTUAL_ENV=${CURDIR}/venv
PYTHON3:=$(PROJECT_VENV_DIRECTORY)/bin/python3 -u -B -W once



.PHONY: venv
.ONESHELL:
venv:
	test -d $(PROJECT_VENV_DIRECTORY) || python3 -m venv $(PROJECT_VENV_DIRECTORY)
	pip3 install --no-cache-dir --upgrade --compile pip
	pip3 install --no-cache-dir --upgrade --compile --requirement requirements.txt


run:
	$(PYTHON3) app1.py
