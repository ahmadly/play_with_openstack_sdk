"""
simple sample code to call openstack api
"""
from pprint import pprint

import ceilometerclient.client
from novaclient import client


def get_nova_client(version=2, username=None, password=None, project_id=None, auth_url=None, **kwargs):
    return client.Client(
        version=version,
        username=username,
        password=password,
        project_id=project_id,
        auth_url=auth_url,
        **kwargs
    )


def get_ceilometer_client(version=2, os_username=None, os_password=None, os_tenant_name=None, os_auth_url=None):
    return ceilometerclient.client.get_client(
        version=version,
        os_username=os_username,
        os_password=os_password,
        os_tenant_name=os_tenant_name,
        os_auth_url=os_auth_url,
    )


def get_servers_list(nova_client):
    """
    get all servers in openstack
    :param nova_client:
    :return:
    """
    return nova_client.servers.list(search_opts={'all_tenants': 1})


pprint(get_servers_list(get_nova_client(username=1, password=1, project_id=1, auth_url=1)))
